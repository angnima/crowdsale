pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "./utils/Crowdsale.sol";
import "./utils/TimedCrowdsale.sol";
import "./utils/CappedCrowdsale.sol";
import "./utils/WhitelistedCrowdsale.sol";

/**
*   @title  ISOContract
*   @dev    Individual Crowdsale Contract per token. Sale of token in exchange of native token (ETH/BNB/MATIC)
*/
contract ISOContract is TimedCrowdsale, CappedCrowdsale {

    bool public finalized;      // Status of crowdsale. True if already ended

    /**
    *   @dev                    Constructor, takes basic details required to set crowdsale contract
    *   @param  _token          address of ERC20 token being sold
    *   @param  _wallet         address of wallet that receives raised ETH
    *   @param  _rate           rate of token per ETH
    *   @param  _hardCap        maximum cap limit of crowdsale
    *   @param  _softCap        minimum cap required to finalize crowdsale
    *   @param  _openingTime    Start time for public crowdsale
    *   @param  _closingTime    Closing time for crowdsale
    */
    constructor (
        IERC20  _token,
        address payable _wallet,
        uint256 _rate,
        uint256 _hardCap,
        uint256 _softCap,
        uint256 _openingTime,
        uint256 _closingTime,
        bool    _whitelistEnabled
    )
        Crowdsale(_token, _wallet, _rate)
        TimedCrowdsale(_openingTime, _closingTime)
        CappedCrowdsale(_hardCap, _softCap)
        WhitelistedCrowdsale(_whitelistEnabled)
    {
        
    }
    
    /**
        Sending directly to the contract makes purchase of token.
        WARNING! Do not overwrite the contract!
    */
    receive() external payable {
        contribute();
    }

}