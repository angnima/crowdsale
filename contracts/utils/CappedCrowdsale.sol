pragma solidity 0.8.10;

import "./Crowdsale.sol";

/**
*   @title          CappedCrowdsale
*   @dev            CappedCrowdsale contract makes it possible to add limit to ETH raised.
*/
contract CappedCrowdsale is Crowdsale {

    uint256 public hardCap;      // Maximimum cap for raising ETH
    uint256 public softCap;      // Minimum cap requirement to finalize crowdsale

    constructor(
        uint256 _hardCap,
        uint256 _softCap
    ) {
        require(_softCap > 0, "CappedCrowdsale: Min Cap must be greater than 0");
        require(_hardCap > _softCap, "CappedCrowdsale: Hardcap must be greater than soft cap.");

        hardCap = _hardCap;
        softCap = _softCap;
    }

    /**
    *   @dev        Check if the cap has been fullfilled to finalize the sales
    *   @return     Returns boolean value of the condition state
    */
    function capReached() public returns (bool) {
        return (ethRaised >= softCap, "CappedCrowdsale: Raised ETH amount must be greater or equals to hardcap.");
    }

    /**
    *   @dev                Function used over various contracts to validate before the purchase of token
    *   @param  _initiator  Address of msg.sender
    *   @param  _ethAmount  Amount of ETH to contribute
    */
    function _preValidateContribution(address _initiator, uint256 _ethAmount) internal {
        super._preValidatePurchase(_initiator, _ethAmount);
        require(ethRaised + _ethAmount <= hardCap, "CappedCrowdsale: hardcap exceeded!");
    } 

}