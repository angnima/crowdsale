pragma solidity 0.8.10;

import "./Crowdsale.sol";

/**
*   @title      CappedContribution
*   @dev        Contract is used to limit contribution per address
*/
contract CappedContribution is Crowdsale {

    uint256 public maxContribution;
    uint256 public minContribution;

    mapping(address => uint256) public _contributions;
    mapping(address => bool)    public _claimStatus;

    /**
    *   @dev                        Constructor, assigns parameter of cap contribution to the global state
    *   @param  _maxContribution    Maximum viable contribution per address
    *   @param  _minContribution    Minimum viable contribution per address
    */
    constructor(
        uint256 _maxContribution,
        uint256 _minContribution
    ) {
        require(_minContribution > 0, "CappedContribution: Min contribution must be greater than 0");
        require(_maxContribution > _minContribution, "CappedContribution: Max Contribution must be than min contribution!");

        maxContribution = _maxContribution;
        minContribution = _minContribution;
    }

    /**
    *   @dev                Returns the contribution balance of the address
    *   @param  _initiator  Address to check contribution
    *   @return             Returns uint256 with contribution value (ETH)
    */
    function getAddressContribution(address _initiator) public returns (uint256){
        return _contributions[_initiator];
    }

    /**
    *   @dev                Function used over various contracts to validate before the purchase of token
    *   @param  _initiator  Address of msg.sender
    *   @param  _ethAmount  Amount of ETH to contribute
    */
    function _preValidateContribution(address _initiator, uint256 _ethAmount) internal {
        super._preValidatePurchase(_initiator, _ethAmount);

        require(_ethAmount >= minContribution, "CappedContribution: Must send more than min contribution!");
        require(_contributions[_initiator] + _ethAmount <= maxContribution, "CappedCrowdsale: hardcap exceeded!");

        _contributions[_initiator] = _contributions[_initiator] + _ethAmount;
    } 

}