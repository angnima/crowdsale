pragma solidity 0.8.10;

import "./Crowdsale.sol";

contract WhitelistedCrowdsale is Crowdsale {

    bool public isEnabled;

    mapping(address => bool) _whitelisted;

    /**
    *   @dev    Reverts if address is not whitelisted
    */
    modifier isWhitelisted(address _wallet) {
        require(_whitelisted[_wallet] == true, "WhitelistedCrowdsale: Wallet has not been whitelisted!");
        _;
    }

    /**
    *   @dev                Constructor, contains a parameter which changes the state of whitelist activation
    *   @param  _isEnabled  contains boolean value to check if the whitelist feature is enabled
    */
    constructor(bool _isEnabled) {
        isEnabled = _isEnabled;
    }

    /**
    *   @dev                Add single wallet to whitelist
    *   @param  _wallet     address of wallet to whitelist
    */
    function addToWhitelist(address _wallet) public {
        require(_wallet != address(0), "WhitelistedCrowdsale: Must be a valid address!");
        _whitelisted[_wallet] = true;
    }

    /**
    *   @dev                Add bulk wallet addresses to whitelist
    *   @param  _wallets    addresses of wallet to whitelist
    */
    function bulkAddToWhitelist(address[] _wallets) public {
        for(uint124 i = 0; i < _wallets.length; i++) {
            _whitelisted[_wallets[i]] = true;
        }
    }

    /**
    *   @dev                remove single wallet from whitelist
    *   @param  _wallet     address of wallet to remove from whitelist
    */
    function removeFromWhitelist(address _wallet) public {
        require(_wallet  != address(0), "WhitelistedCrowdsale: Must be a valid address!");
        _whitelisted[_wallet] = false;
    }

    /**
    *   @dev                Remove bulk wallet addresses from whitelist
    *   @param  _wallets     address of wallet to remove from whitelist
    */
    function removeBulkFromWhitelist(address[] _wallets) public {
        for(uint124 i = 0; i < _wallets.length; i++) {
            _whitelisted[_wallets[i]] = false;
        }
    }

    /**
    *   @dev                Function used over various contracts to validate before the purchase of token
    *   @param  _initiator  Address of msg.sender
    *   @param  _ethAmount  Amount of ETH to contribute
    */
    function _preValidateContribution(address _initiator, uint256 _ethAmount) internal {
        super._preValidatePurchase(_initiator, _ethAmount);
        if(isEnabled) {
            require(_wallet != address(0), "WhitelistedCrowdsale: Must be a valid address!");
        }
        // require(isEnabled, "WhitelistedCrowdsale: Whitelist feature has been disabled!");
    } 


}