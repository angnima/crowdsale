pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/**
*   @title          Crowdsale Contract
*   @dev            State variables, functions required for crowdsale
*/
contract Crowdsale {

    // ======== Start of Global State ========

    // The token being sold
    IERC20 public token;

    address public wallet;      // Address of wallet which receives the ETH raised.

    uint256 public rate;        // Amout of token user can get per ETH
    uint256 public ethRaised;   // Amount of ETH raised

    // ======== End of Global State =========


     // ========== Start of Contract Events ==========

    /**
    *   @notice             Emmitable event when an address purchases token
    *   @param  _by         address of contributer
    *   @param  _value      amount of ETH contributed
    */
    event Contribution(address indexed _by, uint256 _value);

    /**
    *   @notice             Emmitable event when an address withdraws token
    *   @param  _by         address of contributer
    *   @param  _amount     amount of token receivable/received
    */
    event TokenClaimed(address indexed _by, uint256 _amount);

    // ========== End of Contract Events ==========


    /**
    *   @dev                    Constructor, takes basic details required to set crowdsale contract
    *   @param  _token          address of ERC20 token being sold
    *   @param  _wallet         address of wallet that receives raised ETH
    *   @param  _rate           rate of token per ETH
    */
    constructor (
        IERC20  _token,
        address payable _wallet,
        uint256 _rate
    )
    {
        require(_token != address(0),       "ISOContract: Token address must be valid.");
        require(_rate > 0,                  "ISOContract: Rate must be greater than 0");
        require(_wallet != address(0),      "ISOContract: Wallet address cannot be dead address.");
        
        token   = _token;
        wallet  = _wallet;
        rate    = _rate;
    }

    /**
    *   @dev        Contribute function processes purchase of token from crowdsale
    */
    function contribute() public payable {
        uint256 ethAmount = msg.value;

        _preValidateContribution(msg.sender, ethAmount);

        ethRaised += ethAmount;
        emit Contribution(msg.sender, ethAmount);
    }

    /**
    *   @dev        Users can claim tokens according to contribution
    */
    function claimTokens() public {
        require(hasClosed(), "ISOContract: Crowdsale has not ended yet.");
        require(capReached(), "ISOContract: Crowdsale has not reached required cap!");
        require(_contribution[msg.sender] > 0, "ISOContract: No contribuition has been made for token claim!");
        require(_claimStatus[msg.sender] == false, "ISOContract: Token already claimed!");

        uint256 tokenAmount = rate * _contribution[msg.sender];
        _claimStatus[msg.sender] = true;
        token.transfer(msg.sender, tokenAmount);

        emit TokenClaimed(msg.sender, tokenAmount);
    }

    /**
    *   @dev                Function used over various contracts to validate before the purchase of token
    *   @param  _initiator  Address of msg.sender
    *   @param  _ethAmount  Amount of ETH to contribute
    */
    function _preValidateContribution(address _initiator, uint256 _ethAmount) internal {
        require(_ethAmount > 0, "ISOContract: Contribution amount must be greater than 0");
    }

}