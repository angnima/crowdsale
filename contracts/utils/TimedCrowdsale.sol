pragma solidity 0.8.10;

import "./Crowdsale.sol";

/** 
*   @title  TimedCrowdsale
*   @dev    Crowdsale participation is determined by time of opening and closing state.
*/
contract TimedCrowdsale is Crowdsale{

    uint256 public openingTime;
    uint256 public closingTime;

    /**
    *   @dev    Reverts if not in crowdsale time range (crowdsale must be in active state)
    */
    modifier onlyWhileOpen {
        require(block.timestamp >= openingTime && closingTime <= block.timestamp, "TimedCrowdsale: Sale is closed!");
        _;
    }

    /**
    *   @dev                    Constructor, takes opening and closing time as parameter
    *   @param  _openingTime    Crowdsale opening time in unix timestamp
    *   @param  _closingTime    Crowdsale closing time in unix timestamp      
    */
    constructor(
        uint256 _openingTime,
        uint256 _closingTime
    ) {
        require(_openingTime > block.timestamp, "TimedCrowdsale: Starting time must be greater than current time.");
        require(_closingTime > _openingTime, "TimedCrowdsale: Ending time must be greater than starting time.");

        openingTime = _openingTime;
        closingTime = _closingTime;
    }

    /**
    *   @dev                    Checks if the crowdsale time has already ended
    *   @return _isClosed       Returns state of crowdsale in boolean
    */
    function hasClosed() public returns (bool _isClosed){
        _isClosed = block.timestamp > closingTime;
    }

    /**
    *   @dev                Function used over various contracts to validate before the purchase of token
    *   @param  _initiator  Address of msg.sender
    *   @param  _ethAmount  Amount of ETH to contribute
    */
    function _preValidateContribution(address _initiator, uint256 _ethAmount) internal onlyWhileOpen {
        super._preValidateContribution(_initiator, _ethAmount);
    } 

}